package om.sfc.microservices.currencyexchangeservice.api;

import lombok.AllArgsConstructor;
import om.sfc.microservices.currencyexchangeservice.domain.ExchangeValue;
import om.sfc.microservices.currencyexchangeservice.repository.ExchangecurrencyRepository;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import java.math.BigDecimal;
import java.util.List;

@RestController
@AllArgsConstructor
public class ExchangeCurrencyController{

    private final ExchangecurrencyRepository exchangecurrencyRepository;

    @PostMapping(produces =APPLICATION_JSON_VALUE, path = "/currencyexchange/from/{from}/to/{to}")
    public ExchangeValue currencyExchangeFromTo(@PathVariable String from, @PathVariable String to) {
        ExchangeValue ev = ExchangeValue.builder().currencyFrom(from).currencyTo(to).conversionMultiple(BigDecimal.valueOf(50)).build();
        ExchangeValue ev2= exchangecurrencyRepository.save(ev);
        return ev2;
    }

    @GetMapping(produces=APPLICATION_JSON_VALUE, path = "/currencyexchange")
    public List<ExchangeValue> getAllCurrencyExchange(){
        return exchangecurrencyRepository.findAll();
    }
}
