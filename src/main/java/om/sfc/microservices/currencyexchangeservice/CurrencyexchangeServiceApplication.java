package om.sfc.microservices.currencyexchangeservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CurrencyexchangeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CurrencyexchangeServiceApplication.class, args);
	}

}
