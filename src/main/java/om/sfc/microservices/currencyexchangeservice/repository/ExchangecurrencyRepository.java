package om.sfc.microservices.currencyexchangeservice.repository;

import om.sfc.microservices.currencyexchangeservice.domain.ExchangeValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExchangecurrencyRepository extends JpaRepository<ExchangeValue, Long> {

    ExchangeValue save(ExchangeValue exchangeValue);
}
